import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LogInButton extends StatelessWidget {
  final String text;
  final Function press;
  final Color color, textColor;
  final double height, width;
  const LogInButton({
    Key key,
    this.text,
    this.width,
    this.height,
    this.press,
    this.color,
    this.textColor = Colors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      decoration: BoxDecoration(
        color: Colors.grey,
        borderRadius: BorderRadius.circular(50),
//        boxShadow: [
//          BoxShadow(
//            color: Color(0xFFD9664A),
//            offset: Offset(4, 4),
//            blurRadius: 4.0,
//          ),
//        ],
      ),
      margin: EdgeInsets.symmetric(vertical: 10),
      width: size.width * width,
      height: size.height * height,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(50),
        child: RaisedButton(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 40),
          color: Color(0xFFD9664A),
          onPressed: press,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(
                text,
                style: TextStyle(
                  color: textColor,
                  fontSize: 25,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
