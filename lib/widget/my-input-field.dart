import 'package:flutter/material.dart';

class MyInputField extends StatefulWidget {
  final String text;
  const MyInputField({Key key, this.text}) : super(key: key);
  //MyInputField(this.text);
//  MyInputField({Key key, this.text}) : super(key: key);
  @override
  _MyInputFieldState createState() => _MyInputFieldState();
}

class _MyInputFieldState extends State<MyInputField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
        labelText: widget.text,
        labelStyle: TextStyle(
          color: Color(0xFF797979),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Color(0xFF797979)),
        ),
      ),
    );
  }
}
