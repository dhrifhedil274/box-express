import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ScanPage extends StatefulWidget {
  @override
  _ScanPageState createState() => _ScanPageState();
}

class _ScanPageState extends State<ScanPage> {
  var qrCodeResult = "Not Yet Scanned";

//  Future _scanQR() async {
//    try {
//      var qrResult = await BarcodeScanner.scan();
//      print('Response: ${qrResult}');
//      setState(() {
//        var result = qrResult.rawContent;
//      });
//      final http.Response response = await http.post(
//          'https://smart-event-api.herokuapp.com/api/consultations/',
//          body: {'code': result},
//          headers: {'Authorization': _authToken});
//      final responseJson = json.decode(response.body);
//      print('Response: ${responseJson}');
//    } on PlatformException catch (ex) {
//      if (ex.code == BarcodeScanner.CameraAccessDenied) {
//        setState(() {
//          result = "Camera permission was denied";
//        });
//      } else {
//        setState(() {
//          result = "Unknown Error $ex";
//        });
//      }
//    } catch (ex) {
//      setState(() {
//        result = "Unknown Error $ex";
//      });
//    }
//  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Scanner"),
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(
              "Result",
              style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            Text(
              qrCodeResult,
              style: TextStyle(
                fontSize: 20.0,
              ),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 20.0,
            ),
            FlatButton(
              padding: EdgeInsets.all(15.0),
              onPressed: () async {
                var codeSanner = await BarcodeScanner.scan(); //barcode scnner
                setState(() {
                  qrCodeResult = codeSanner.rawContent;
                });
                print(codeSanner.rawContent);
                // try{
                //   BarcodeScanner.scan()    this method is used to scan the QR code
                // }catch (e){
                //   BarcodeScanner.CameraAccessDenied;   we can print that user has denied for the permisions
                //   BarcodeScanner.UserCanceled;   we can print on the page that user has cancelled
                // }
              },
              child: Text(
                "Open Scanner",
                style:
                    TextStyle(color: Colors.blue, fontWeight: FontWeight.bold),
              ),
              shape: RoundedRectangleBorder(
                  side: BorderSide(color: Colors.blue, width: 3.0),
                  borderRadius: BorderRadius.circular(20.0)),
            )
          ],
        ),
      ),
    );
  }

//its quite simple as that you can use try and catch staatements too for platform exception
}
