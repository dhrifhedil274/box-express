import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../bloc.navigation_bloc/navigation_bloc.dart';
import 'package:sidebar_animation/widget/my-input-field.dart';
import 'package:sidebar_animation/widget/login-button.dart';

class MyAccountsPage extends StatefulWidget with NavigationStates {
  @override
  _MyAccountsPageState createState() => _MyAccountsPageState();
}

class _MyAccountsPageState extends State<MyAccountsPage> {
  //FocusNode myFocusNode = new FocusNode();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF6F9F9),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              Center(
                child: Container(
                  margin: EdgeInsets.only(top: 60),
                  height: 150,
                  width: 150,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                  ),
                  child: CircleAvatar(
                    backgroundImage: AssetImage('assets/profile-picture.jpeg'),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(50),
                child: Form(
                  child: Column(
                    children: [
                      MyInputField(
                        text: "User Name",
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      MyInputField(
                        text: "User Phone",
                      ),
                      SizedBox(
                        height: 20,
                      ),
                     MyInputField(
                       text: "User E-mail",
                     ),
                      SizedBox(
                        height: 20,
                      ),
                      MyInputField(
                        text: "User Password",
                      ),
                      SizedBox(height: 30,),
                      LogInButton(
                        press: (){},
                        text: "Edit Profile",
                        height: 0.08,
                        width: 0.6,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
