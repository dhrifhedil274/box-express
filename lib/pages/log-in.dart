import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sidebar_animation/sidebar/sidebar_layout.dart';
import 'package:sidebar_animation/widget/inputwithicon.dart';
import 'package:sidebar_animation/widget/login-button.dart';
import 'package:sidebar_animation/api/api.dart';
import 'dart:convert';

class LogIn extends StatefulWidget {
  @override
  _LogInState createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  bool _isLoading = false;

  final _formKey = GlobalKey<FormState>();

  final emailcontroller = TextEditingController();
  final passwordcontroller = TextEditingController();
  String message;

  @override
  void dispose() {
    emailcontroller.dispose();
    passwordcontroller.dispose();
    super.dispose();
  }

//  ScaffoldState scaffoldState;
//  _showMsg(msg) {
//    //
//    final snackBar = SnackBar(
//      content: Text(msg),
//      action: SnackBarAction(
//        label: 'Close',
//        onPressed: () {
//          // Some code to undo the change!
//        },
//      ),
//    );
//    Scaffold.of(context).showSnackBar(snackBar);
//  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF6F9F9),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin:
                  EdgeInsets.only(top: 100, left: 30, right: 30, bottom: 70),
              child: Image(
                image: AssetImage('assets/delivery.png'),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 50),
              child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 60,
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            offset: Offset(2, 3),
                            blurRadius: 4.0,
                            color: Colors.grey[400],
                          ),
                        ],
                        //border: Border.all(color: Color(0xFFBC7C7C7), width: 2),
                        borderRadius: BorderRadius.circular(50),
                        color: Colors.white,
                      ),
                      child: Row(
                        children: <Widget>[
                          Container(
                              width: 60,
                              child: Icon(
                                Icons.email,
                                size: 20,
                                color: Color(0xFFBB9B9B9),
                              )),
                          Expanded(
                            child: TextFormField(
                              controller: emailcontroller,
                              decoration: InputDecoration(
                                  contentPadding:
                                      EdgeInsets.symmetric(vertical: 15),
                                  border: InputBorder.none,
                                  hintText: "Enter Email ..."),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return "Email required";
                                }
                                return null;
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    InputWithIcon(
                      obscure: true,
                      valid: (value) {
                        if (value.isEmpty) {
                          return "Correct password is required";
                        }
                        return null;
                      },
                      controller: passwordcontroller,
                      icon: Icons.vpn_key,
                      hint: "Enter Password...",
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    LogInButton(
                      height: 0.07,
                      width: 0.8,
                      text: _isLoading ? 'Loging...' : 'Login',
                      press: () {
                        if(_formKey.currentState.validate())
                          {
                            var email = emailcontroller;
                            var password = passwordcontroller;
                            setState(() {
                               message="Please wait";
                            });
                          }
                      },
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _login() async {
    setState(() {
      _isLoading = true;
    });

    var data = {
      'email': emailcontroller.text,
      'password': passwordcontroller.text
    };

    var res = await CallApi().postData(data, 'login');
    var body = json.decode(res.body);
    //if (body['success']) {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.setString('token', body['token']);
    localStorage.setString('user', json.encode(body['user']));
    Navigator.push(
        context, new MaterialPageRoute(builder: (context) => SideBarLayout()));
    //} else {
    //_showMsg(body['message']);
    // }

    setState(() {
      _isLoading = false;
    });
  }
}
