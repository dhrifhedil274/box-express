import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sidebar_animation/pages/log-in.dart';
import 'package:sidebar_animation/sidebar/sidebar_layout.dart';
import 'dart:async';

class MySplashScreen extends StatefulWidget {
  @override
  _MySplashScreenState createState() => _MySplashScreenState();
}

class _MySplashScreenState extends State<MySplashScreen> {
  bool _isLoggedIn;

  @override
  void _checkIfLoggedIn() async {
    // check if token is t here
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var token = localStorage.getString('token');
    if (token != null) {
      print(token);
      setState(() {
        _isLoggedIn = true;
      });
    }
    print(_isLoggedIn);
  }

  void initState() {
    _checkIfLoggedIn();
    print(_isLoggedIn);
//    _mockCheckForSession().then((status) {
//      if (status) {
//        navigateUser();
//      }
//    });

    startTimer();
    super.initState();
  }

  void startTimer() {
    Timer(Duration(seconds: 3), () {
      navigateUser(); //It will redirect  after 3 seconds
    });
  }

  void navigateUser() async {
//    SharedPreferences prefs = await SharedPreferences.getInstance();
//    var status = prefs.getBool('isLoggedIn') ?? false;
//    print(status);
    //_checkIfLoggedIn();

    if (_isLoggedIn == true) {
      Navigator.of(context).pushReplacementNamed("/HomePage");
    } else {
      Navigator.of(context).pushReplacementNamed("/Login");
    }
  }

//  void navigateUser() async {
//    SharedPreferences prefs = await SharedPreferences.getInstance();
//
//    var status = prefs.getBool('isLoggedIn') ?? false;
//    print(status);
//    if (status) {
//      Navigator.of(context).pushReplacementNamed('/HomePage');
//    } else {
//      Navigator.of(context).pushReplacementNamed('/Login');
//    }
//  }
//
//  Future<bool> _mockCheckForSession() async {
//    await Future.delayed(Duration(milliseconds: 1500), () {});
//
//    return true;
//  }

//  void _navigateToHome() {
//    Navigator.of(context).pushReplacement(
//      MaterialPageRoute(
//        builder: (BuildContext context) => SideBarLayout(),
//      ),
//    );
//  }

//  void _navigateToLogin() {
//    Navigator.of(context).pushReplacement(
//      MaterialPageRoute(
//        builder: (BuildContext context) => LogIn(),
//      ),
//    );
//  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [Color(0xFF3B988E), Color(0xFFFCCA73)],
              ),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: Container(
                  width: size.width * 0.8,
                  child: Image(
                    image: AssetImage('assets/logo2.png'),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
