import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sidebar_animation/pages/scan.dart';
import '../bloc.navigation_bloc/navigation_bloc.dart';
import 'package:sidebar_animation/widget/rounded_button.dart';
import 'myaccountpage.dart';
import 'package:flutter/services.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'scan.dart';
class HomePage extends StatefulWidget with NavigationStates {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

//    _scanQR() async {
//    try {
//      String qrResult = await BarcodeScanner.scan().toString();
//      setState(() {
//        result = qrResult;
//      });
//      print(result);
//    } on PlatformException catch (ex) {
//      if (ex.code == BarcodeScanner.cameraAccessDenied) {
//        setState(() {
//          result = "Camera permission was denied";
//        });
//      } else {
//        setState(() {
//          result = "Unknown Error $ex";
//        });
//      }
//    } on FormatException {
//      setState(() {
//        result = "You pressed the back button before scanning anything";
//      });
//    } catch (ex) {
//      setState(() {
//        result = "Unknown Error $ex";
//      });
//    }
//  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Color(0xFFF6F9F9),
      body: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 80, left: 40),
            width: size.width * 0.7,
            child: Image(image: AssetImage('assets/logo1.png')),
          ),
          Container(
            margin: EdgeInsets.only(top: 60),
            child: Padding(
              padding: const EdgeInsets.only(left: 45),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RoundedButton(
                    height: 0.16,
                    image: 'assets/delivered.png',
                    text: "Delivered",
                    press: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ScanPage()),
                      );
                    },
                  ),
                  RoundedButton(
                    height: 0.16,
                    image: 'assets/returned.png',
                    text: "Returned",
                    press: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MyAccountsPage()),
                      );
                    },
                  ),
                  RoundedButton(
                    height: 0.16,
                    image: 'assets/info-23333.png',
                    text: "More Info",
                    press: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MyAccountsPage()),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
