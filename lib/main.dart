import 'package:flutter/material.dart';
import 'package:sidebar_animation/pages/homepage.dart';
import 'package:sidebar_animation/pages/log-in.dart';
import 'package:sidebar_animation/sidebar/sidebar_layout.dart';
import 'pages/splach-screen.dart';
import 'pages/log-in.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          scaffoldBackgroundColor: Colors.white, primaryColor: Colors.white),
      home: MySplashScreen(),
      routes: <String, WidgetBuilder>{
        '/Login': (BuildContext context) => new LogIn(),
        '/HomePage': (BuildContext context) => new SideBarLayout(),
      },
    );
  }
}
